﻿namespace Simple_Arithmetic_Operations {
    partial class Form1 {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.lbl_FirstNumber = new System.Windows.Forms.Label();
            this.lbl_SecondNumber = new System.Windows.Forms.Label();
            this.txt_FirstNumber = new System.Windows.Forms.TextBox();
            this.txt_SecondNumber = new System.Windows.Forms.TextBox();
            this.btn_Add = new System.Windows.Forms.Button();
            this.Btn_Quit = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbl_FirstNumber
            // 
            this.lbl_FirstNumber.AutoSize = true;
            this.lbl_FirstNumber.Location = new System.Drawing.Point(139, 74);
            this.lbl_FirstNumber.Name = "lbl_FirstNumber";
            this.lbl_FirstNumber.Size = new System.Drawing.Size(135, 25);
            this.lbl_FirstNumber.TabIndex = 0;
            this.lbl_FirstNumber.Text = "First Number";
            // 
            // lbl_SecondNumber
            // 
            this.lbl_SecondNumber.AutoSize = true;
            this.lbl_SecondNumber.Location = new System.Drawing.Point(139, 117);
            this.lbl_SecondNumber.Name = "lbl_SecondNumber";
            this.lbl_SecondNumber.Size = new System.Drawing.Size(166, 25);
            this.lbl_SecondNumber.TabIndex = 1;
            this.lbl_SecondNumber.Text = "Second Number";
            // 
            // txt_FirstNumber
            // 
            this.txt_FirstNumber.Location = new System.Drawing.Point(367, 74);
            this.txt_FirstNumber.Name = "txt_FirstNumber";
            this.txt_FirstNumber.Size = new System.Drawing.Size(100, 31);
            this.txt_FirstNumber.TabIndex = 2;
            // 
            // txt_SecondNumber
            // 
            this.txt_SecondNumber.Location = new System.Drawing.Point(367, 117);
            this.txt_SecondNumber.Name = "txt_SecondNumber";
            this.txt_SecondNumber.Size = new System.Drawing.Size(100, 31);
            this.txt_SecondNumber.TabIndex = 3;
            // 
            // btn_Add
            // 
            this.btn_Add.Location = new System.Drawing.Point(144, 343);
            this.btn_Add.Name = "btn_Add";
            this.btn_Add.Size = new System.Drawing.Size(93, 36);
            this.btn_Add.TabIndex = 4;
            this.btn_Add.Text = "Add";
            this.btn_Add.UseVisualStyleBackColor = true;
            this.btn_Add.Click += new System.EventHandler(this.btn_Add_Click);
            // 
            // Btn_Quit
            // 
            this.Btn_Quit.Location = new System.Drawing.Point(608, 343);
            this.Btn_Quit.Name = "Btn_Quit";
            this.Btn_Quit.Size = new System.Drawing.Size(93, 36);
            this.Btn_Quit.TabIndex = 5;
            this.Btn_Quit.Text = "Quit";
            this.Btn_Quit.UseVisualStyleBackColor = true;
            this.Btn_Quit.Click += new System.EventHandler(this.Btn_Quit_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.Btn_Quit);
            this.Controls.Add(this.btn_Add);
            this.Controls.Add(this.txt_SecondNumber);
            this.Controls.Add(this.txt_FirstNumber);
            this.Controls.Add(this.lbl_SecondNumber);
            this.Controls.Add(this.lbl_FirstNumber);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_FirstNumber;
        private System.Windows.Forms.Label lbl_SecondNumber;
        private System.Windows.Forms.TextBox txt_FirstNumber;
        private System.Windows.Forms.TextBox txt_SecondNumber;
        private System.Windows.Forms.Button btn_Add;
        private System.Windows.Forms.Button Btn_Quit;
    }
}

